<?php

require_once "../classes/RabbitMQ.php";
require_once "../config/rabbit.php";

class Consumer {
	
	function run(){
		$oRabbit = new RabbitMQ(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER, RABBITMQ_PASS, RABBITMQ_VHOST);
		$oRabbit->setExchangeName(RABBITMQ_EXCHANGE);
		$oRabbit->setExchangeType(); // direct di default
		$oRabbit->setQueueName(RABBITMQ_QUEUE);
		$oRabbit->setPayload(array($this, 'consumerQueue'), array());
		$oRabbit->consumer();
		$oRabbit->ack("OK");
	}		
	
	function consumerQueue($aQueue){
        var_dump($aQueue);
	}

}

$oScan = new Consumer();
$oScan->run();
