<?php

require_once('RabbitMQ/amqp.inc');


/**
 * Class RabbitMQ
 * @author Sergio Sicari
 */
class RabbitMQ{

	private $_oConnection = null;
	private $_oChannel = null;
	private $_aExchange = array();
	private $_sExchangeName = '';
	private $_sExchangeType = '';
	private $_sQueueName = '';
	private $_aCallback = array();
	private $_sRoutingKey = '';
	private $_sConsumerTag = '';

	const EX_FANOUT = 'fanout';
	const EX_DIRECT = 'direct';
	const EX_TOPIC  = 'topic';

	function __construct($sHost = 'localhost', $nPort = 5672, $sUser  = "guest", $sPass  = "guest", $sVhost = "/"){
		//echo "Creating connection ..." . PHP_EOL;
		$this->_oConnection = new AMQPConnection($sHost, $nPort, $sUser, $sPass, $sVhost);
		//echo "Get channel ..." . PHP_EOL;
		$this->_oChannel = $this->_oConnection->channel();
	}

	public function setExchangeName($sName){
		$this->_sExchangeName = $sName;
	}

	public function setExchangeType($sType = self::EX_DIRECT){
		$this->_sExchangeType = $sType;
	}

	public function setQueueName($sName){
		$this->_sQueueName = $sName;
	}

	public function setPayload($sCallback, $aArgs = array()){
		$this->_aCallback = array("function" => $sCallback, "args" => $aArgs);
	}

	public function publisher($sPayload, $sTags = null){
		$this->_setExchange($this->_sExchangeName, $this->_sExchangeType);
		$message = new AMQPMessage( $sPayload, array( 'content_type' => 'application/json', 'delivery_mode' => 2));
		try{
			$this->_oChannel->basic_publish($message, $this->_sExchangeName, $sTags, true);
		}catch(Exception $e){
			return $e->getMessage("ERROR: Could not publish message");
		}
	}

    public function consumer(){
		$this->_setQueue($this->_sQueueName, $this->_sExchangeName, $this->_sExchangeType, $this->_sRoutingKey);
		$this->_oChannel->basic_consume($this->_sQueueName, $this->_sConsumerTag, false, false, false, false, array($this, 'messageProcessor'));
		while(count($this->_oChannel->callbacks)) {
    		$this->_oChannel->wait();
		}
	}

	public function ack($sDelivery){
		$this->_oChannel->basic_ack($sDelivery);
	}

	private function _setExchange($sName, $sType, $bPassive = false){
		try {
			//exchange_declare here is PASSIVE by default, meaning that the exchange MUST be declared before we invoke this method.
			$this->_oChannel->exchange_declare($sName, $sType, $bPassive, true, false);
			$this->_aExchange[$sName] = $sType;
		} catch (AMQPChannelException $e) {
			$e->getMessage("ERROR: Could not declare exchange '$sName' with type $sType");
			throw $e;
		}
	}

	private function _setQueue($sQueueName, $sExchangeName, $sExchangeType, $sRoutingKey){
		$this->_setExchange($sExchangeName, $sExchangeType);
		$this->_oChannel->queue_declare($sQueueName, false, true, false, false);
		echo ' [*] Waiting for message. To exit press CTRL+C';
		$this->_oChannel->queue_bind($sQueueName, $sExchangeName);
	}

	public function messageProcessor($msg){
		$oMsg = json_decode($msg->body, true); // true per avere l' array associativo ... da rimettere a posto (senza true per trattare oggetti ...
		$aAllParams = array_merge(array($oMsg), $this->_aCallback['args']);
		call_user_func_array($this->_aCallback['function'], $aAllParams);
		$this->_oChannel->basic_ack($msg->delivery_info['delivery_tag']);
	}


	
}
