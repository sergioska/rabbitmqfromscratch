<?php

/**
 * RabbitMQ config parameters
 */

define ('RABBITMQ_HOST', 'localhost');
define ('RABBITMQ_PORT', 5672);
define ('RABBITMQ_USER', 'sicari');
define ('RABBITMQ_PASS', 'sergio');
define ('RABBITMQ_EXCHANGE', 'router');
define ('RABBITMQ_VHOST' ,'localhost');
define ('RABBITMQ_QUEUE', 'durable_hello');
define ('RABBITMQ_CONSUMER_TAG', 'consumer');
