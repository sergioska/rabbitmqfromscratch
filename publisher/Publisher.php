<?php

require_once('../classes/RabbitMQ.php');
require_once('../config/rabbit.php');

class Publisher{

    protected $_oRabbit;
	
	function __construct(){
		$this->_oRabbit = new RabbitMQ(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER, RABBITMQ_PASS, RABBITMQ_VHOST);
		$this->_oRabbit->setExchangeName(RABBITMQ_EXCHANGE);
		$this->_oRabbit->setExchangeType();
	}
	
	function loadImages($aImages) {
		$aMsg = array("body" => array("images" => $aImages), "delivery_info" => "[OK] Downloaded");
		$sJsonMsg = json_encode($aMsg);
		$this->_oRabbit->publisher($sJsonMsg);
		
	}
	
}

$aImages = array("http://cdn.home-designing.com/wp-content/uploads/2009/03/childrens-room-4.jpg",
                 "http://cdn.home-designing.com/wp-content/uploads/2009/03/kids-room-design1.jpg",
                 "http://i.supelex.ru/u/20/2c2282124011e3afd913ffb4853424/-/026.jpg");

$oPublisher = new Publisher();
$oPublisher->loadImages($aImages);
